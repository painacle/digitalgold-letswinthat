//
//! \file
//
// Created by Sander van Woensel / Umut Uyumaz
// Copyright (c) 2018 ASML Netherlands B.V. All rights reserved.
//
//! Example Task to output something to the LED Matrix.
#include "Tasks_ExampleDisplayTask.hpp"

#include "Debug.hpp"
#include "Facilities_MeshNetwork.hpp"

#include <LEDMatrixDriver.hpp>
#include <vector>
#include <functional>
#include <chrono>
#include <ctime>
#include <map>

String requestString;

namespace Tasks {

const int ExampleDisplayTask::SIZE = 8;
const int ExampleDisplayTask::LEDMATRIX_WIDTH = 32;
const int ExampleDisplayTask::LEDMATRIX_HEIGHT = 8;
const int ExampleDisplayTask::LEDMATRIX_SEGMENTS = 4;
const int ExampleDisplayTask::LEDMATRIX_INTENSITY = 6;
const int ExampleDisplayTask::LEDMATRIX_CS_PIN = 16;
const unsigned long ExampleDisplayTask::POLL_DELAY_MS = 100;

//! Initializes the LED Matrix display.
ExampleDisplayTask::ExampleDisplayTask(Facilities::MeshNetwork& mesh) :
   Task(POLL_DELAY_MS , TASK_FOREVER, std::bind(&ExampleDisplayTask::execute, this)),
   m_mesh(mesh),
   m_lmd(LEDMATRIX_SEGMENTS, LEDMATRIX_CS_PIN),
   m_x(0)
{
   m_lmd.setEnabled(true);
   m_lmd.setIntensity(LEDMATRIX_INTENSITY);

   m_mesh.onReceive(std::bind(&ExampleDisplayTask::receivedCb, this, std::placeholders::_1, std::placeholders::_2));
}

void ExampleDisplayTask::Display(vector<vector<int>> mx, int blockId) {
    //assert(mx.size() == LEDMATRIX_WIDTH);
    //assert(mx[0].size() == LEDMATRIX_HEIGHT);

    m_lmd.clear();
    for(uint16_t x = 0; x < mx.size(); x++) {
        // transmute x 
        int xBucket = x / SIZE;
        int xOffset = SIZE - 1 - x % SIZE;
        int xLed = xBucket * SIZE + xOffset;
        for(uint16_t y = blockId * SIZE; y < (blockId + 1) * SIZE; y++) {
            int yLed = y % SIZE;
            m_lmd.setPixel(xLed, yLed, mx[x][y]);
        }
    }
    m_lmd.display();
}

vector<vector<int>> ExampleDisplayTask::GenerateMx(int totalBlocks) {
    vector<vector<int>> mx(LEDMATRIX_WIDTH, vector<int>(totalBlocks * LEDMATRIX_HEIGHT, 0));
    double cx = SIZE * 2 + 0.5;
    double cy = totalBlocks * SIZE / 2 - 0.5;
    double r = totalBlocks * SIZE / 2;
    for(int x = 0; x < mx.size(); x++) {
        for(int y = 0; y < mx[x].size(); y++) {
            if((cx - x) * (cx - x) + (cy - y) * (cy - y) <= r * r) mx[x][y] = 1;
            else mx[x][y] = 0;
        }
    }
    return mx;
}

vector<vector<int>> ExampleDisplayTask::resize(vector<vector<int>> mx, int thumbwidth, int thumbheight) {
	int width = mx.size();
	int height = mx[0].size();
	vector<vector<int>> res(thumbwidth, vector<int>(thumbheight, 0));
	
	double xscale = (thumbwidth + 0.0) / width;
	double yscale = (thumbheight + 0.0) / height;
	double threshold = 0.5 / (xscale * yscale);
	double yend = 0.0;
	for (int f = 0; f < thumbheight; f++) // y on output
	{
	    double ystart = yend;
	    yend = (f + 1) / yscale;
	    if (yend >= height) yend = height - 0.000001;
	    double xend = 0.0;
	    for (int g = 0; g < thumbwidth; g++) // x on output
	    {
	        double xstart = xend;
	        xend = (g + 1) / xscale;
	        if (xend >= width) xend = width - 0.000001;
	        double sum = 0.0;
	        for (int y = (int)ystart; y <= (int)yend; ++y)
	        {
	            double yportion = 1.0;
	            if (y == (int)ystart) yportion -= ystart - y;
	            if (y == (int)yend) yportion -= y+1 - yend;
	            for (int x = (int)xstart; x <= (int)xend; ++x)
	            {
	                double xportion = 1.0;
	                if (x == (int)xstart) xportion -= xstart - x;
	                if (x == (int)xend) xportion -= x+1 - xend;
	                sum += mx[y][x] * yportion * xportion;
	            }
	        }
	        res[f][g] = (sum > threshold) ? 1 : 0;
	    }
	}
	return res;
}


//! Update display
void ExampleDisplayTask::execute()
{
    //MY_DEBUG_PRINTLN("RS\n" + requestString);
    mx = vector<vector<int>>(32, vector<int>(32, 0));
    for(int i = 0; i < requestString.length(); i++) {
        int x = i / 32, y = i % 32;
        mx[x][y] = requestString[i] == '0' ? 0 : 1;
    }
    //auto nodes = m_mesh.getNodeList();

    auto mxToDisplay = mx;
    mxToDisplay = resize(mxToDisplay, 8, 8); 

    auto center = [&](vector<vector<int>> &mx) -> bool {
        int h = mx[0].size();
        for(int i = 0; i < (32 - h) / 2; i++) mx.insert(mx.begin(), vector<int>(h, 0));
    };
    center(mxToDisplay);
    
    //mx = GenerateMx(4);
    Display(mxToDisplay, 0);
}

void ExampleDisplayTask::receivedCb(Facilities::MeshNetwork::NodeId nodeId, String& msg)
{
    // MY_DEBUG_PRINTLN("Received data in ExampleDisplayTask" + msg);
    // if(msg[0] == 'I') {
    //     mx = vector<vector<int>>(32, vector<int>(32, 0));
    //     for(int i = 1; i < msg.length(); i++) {
    //         int id = i - 1;
    //         int x = id / 32, y = id % 32;
    //         mx[x][y] = msg[i] == '0' ? 0 : 1;
    //     }
    // }
}

} // namespace Tasks
