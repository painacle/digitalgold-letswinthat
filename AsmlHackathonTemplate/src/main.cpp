#include <Arduino.h>

#include "Debug.hpp"
#include "painlessMesh.h"
#include "Facilities_MeshNetwork.hpp"
#include "Tasks_ExampleTransmitTask.hpp"
#include "Tasks_ExampleDisplayTask.hpp"
#include "SoftwareSerial.h"
#include <ESP8266WiFi.h>


// Translation unit local variables
namespace {

Scheduler                  taskScheduler;

Facilities::MeshNetwork    meshNetwork;
Tasks::ExampleTransmitTask exampleTransmitTask(meshNetwork);
Tasks::ExampleDisplayTask  exampleDisplayTask(meshNetwork);
}

// Replace with your network credentials
String ssid     = "Rzeczypospolita";
String password = "lolololo";


// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

void initWiFiServer()
{
  IPAddress ip(10,160,53,113); // this 3 lines for a fix IP-address
  IPAddress gateway(10,160,53,1);
  IPAddress subnet(255, 255, 255, 0);

  WiFi.config(ip, gateway, subnet); // before or after Wifi.Begin(ssid, password);
  // Connect to Wi-Fi network with SSID ansd password
  Serial.print("Connecting to ");
  Serial.println(ssid.c_str());
  WiFi.begin(ssid.c_str(), password.c_str());
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();

}

//! Called once at board startup.
void setup()
{
   MY_DEBUG_BEGIN(115200);
  
   meshNetwork.initialize(F("Rzeczypospolita"), F("lolololo"), taskScheduler);

   initWiFiServer();

   // Create and add tasks.
   taskScheduler.addTask( exampleTransmitTask );
   taskScheduler.addTask( exampleDisplayTask );
   exampleTransmitTask.enable();
   exampleDisplayTask.enable();

   MY_DEBUG_PRINTLN(F("Setup completed"));
}

void receiveWebRequest()
{
  
   if(Serial.available())
    {
        char c = Serial.read();
        MY_DEBUG_PRINTF("SerialInput! %c\r\n",c);
        MY_DEBUG_PRINTLN(WiFi.localIP());
    }
  WiFiClient client = server.available();   // Listen for incoming clients

  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String x;
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {   
        x = client.readString();         // if there's bytes to read from the client,
        int beg = 0;
        
        for(beg = 0; beg < (int)x.length() - 1; beg++) {
          if(x[beg] == '\n' && x[beg + 1] == '\n') 
          {
            beg += 2;
            break;
          }
          else if(x[beg]=='\r' 
          && beg<(int)x.length()+3 
          && x[beg+1]=='\n'
          && x[beg+2]=='\r'
          && x[beg+3]=='\n')
            {
            beg+=4;
            break;
            }
        }
        x = x.substring(beg);
        Serial.print(x);
        requestString = x;
        break;
      }
    }
      Serial.println("Received message"+header);
        client.println("OK");
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
    //meshNetwork.sendBroadcast("I"+x); // false: Do not include self.
  }
}

//! Called repeatedly after setup().
void loop()
{
   taskScheduler.execute();
   meshNetwork.update();
   receiveWebRequest();
}