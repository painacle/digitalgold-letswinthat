(function (module) {
    "use strict";
    var MainCtrl = function ($window, $http) {
        var vm = this;

        vm.HOSTS = ["http://10.160.53.106:80",
            "http://10.160.53.173:80",
            "http://10.160.53.174:80",
            "http://10.160.53.184:80"
        ];

        vm.rows = 32;
        vm.columns = 32;

        vm.update = function () {
            const message = vm.stringify(vm.matrix);
            for(var i = 0; i < vm.HOSTS.length; ++i) {
                var address = vm.HOSTS[i] + "/update";
                console.log("Sending to: " + address);
                $http({
                    url: address,
                    method: "POST",
                    data: message
                }).then(function (response) {
                        // success
                    },
                    function (response) { // optional
                        // failed
                    });
            }
        };

        vm.drawCircle = function(isSmall, isEmpty) {
            const midX = vm.matrix.length / 2 - 0.5;
            const midY = vm.matrix[0].length / 2 - 0.5;
            const radius = isSmall ? 5 : (Math.min(vm.matrix.length, vm.matrix[0].length) / 2 - 1);
            for(var i = 0; i < vm.matrix.length; ++i) {
                for(var j = 0; j < vm.matrix[i].length; ++j) {
                    if(isEmpty) {
                        vm.matrix[i][j] = Math.abs(vm.getDist(i, j, midX, midY) - radius) < 0.0001;
                    } else {
                        vm.matrix[i][j] = vm.getDist(i, j, midX, midY) <= radius;
                    }
                }
            }
            vm.update();
        };

        vm.fillBoard = function(value) {
            for(var i = 0; i < vm.matrix.length; ++i) {
                for(var j = 0; j < vm.matrix[i].length; ++j) {
                    vm.matrix[i][j] = value;
                }
            }
            vm.update();
        };

        vm.getDist = function(x1, y1, x2, y2) {
            return Math.floor(Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));
        };

        vm.stringify = function(matrix) {
            var result = "";
            for(var i = 0; i < matrix.length; ++i) {
                for(var j = 0; j < matrix.length; ++j) {
                    result += (matrix[i][j] ? "1" : "0");
                }
            }
            return encodeURIComponent(result);
        };

        vm.init = function (vm) {
            $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded;charset=utf-8;";

            vm.matrix = new Array(vm.rows);

            for (var i = 0; i < vm.matrix.length; ++i) {
                vm.matrix[i] = new Array(vm.columns);
                for (var j = 0; j < vm.matrix[i].length; ++j) {
                    vm.matrix[i][j] = false;
                }
            }

            vm.update();

            setInterval(function () {
                vm.update();
            }, 60000);
        };

        vm.getNumber = function (num) {
            var arr = new Array(num);
            for (var i = 0; i < arr.length; ++i) {
                arr[i] = i;
            }
            return arr;
        };

        vm.toggle = function (row, column) {
            vm.matrix[row][column] = !vm.matrix[row][column];
        };

        vm.getButtonColor = function (row, column) {
            return (vm && vm.matrix && vm.matrix[row][column]) ? '#FF0000' : '#FFFFFF';
        };

        $window.onload = function () {
            vm.init(vm);
        };
    };

    module.controller("MainCtrl", MainCtrl);

}(angular.module("digitalGold")));