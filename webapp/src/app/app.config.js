(function (module) {

    var eventPlannerConfig = function ($routeProvider, $locationProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "/views/homepage.html",
                controller: "MainCtrl",
                controllerAs: "vm"
            })
            .otherwise({
                redirectTo: "/homepage"
            });
        $locationProvider.html5Mode(true);
    };

    module.config(eventPlannerConfig);

}(angular.module("digitalGold")));